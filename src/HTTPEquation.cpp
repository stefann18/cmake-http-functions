#pragma once
#include "HTTPEquation.h"
#include "asio.hpp"


typedef std::pair<std::string, std::string>(*ScriptFunction)(std::string); // function pointer type
typedef std::map<std::string, ScriptFunction> script_map;


FunctionsMap FM;

std::pair<std::string, std::string> FunctionsMap::call_script(const std::string& pFunction, std::string token)
{
	std::pair<std::string, std::string> p;
	script_map::const_iterator iter = map.find(pFunction);
	if (iter == map.end())
	{
		p.first = "404";
		return p;
	}
	else {
		p = (*iter->second)(token);
		return p;
	}
}


using asio::ip::tcp;

class ExtractInfo {
public:
	static std::string ExtractInfoPost(char data_[1024], std::string &httpV, std::string &host) {
		char *aux = "";
		std::string delimiter = "\r\n";
		std::string s(data_);
		size_t pos = 0;
		std::string token;
		std::cout << std::endl;

		pos = s.find("/"); s.erase(0, pos + 1); pos = s.find("/");
		host = s.substr(0, s.find(" "));
		token = s.substr(pos + 1, pos + 4);
		httpV = token.substr(0, 3);
		pos = s.find(delimiter);
		s.erase(0, pos + delimiter.length());

		pos = s.find(delimiter);
		token = s.substr(0, pos); pos = token.find(":");
		token = token.substr(pos + 2, token.size());
		host = token + "/" + host;
		while ((pos = s.find(delimiter)) != std::string::npos) {
			token = s.substr(0, pos);
			s.erase(0, pos + delimiter.length());
		}
		pos = s.find((char)(-51));
		token = s.substr(0, pos);
		return token;
	}
	static std::string ExtractInfoGet(char data_[1024], std::string &httpV, std::string &host) {
		char *aux = ""; int nr = 0;
		std::string delimiter = "\r\n";
		std::string s(data_);
		size_t pos = 0;
		std::string token, dummy = s, auxS;

		pos = dummy.find("/"); dummy.erase(0, pos + 1); pos = dummy.find("/");
		host = dummy.substr(0, dummy.find(" "));
		auxS = host.substr(host.find("=") + 1, host.size());
		token = dummy.substr(pos + 1, pos + 4);
		httpV = token.substr(0, 3);
		pos = dummy.find(delimiter);
		dummy.erase(0, pos + delimiter.length());

		pos = dummy.find(delimiter);
		token = dummy.substr(0, pos); pos = token.find(":");
		token = token.substr(pos + 2, token.size());

		host = token + "/" + host;
		std::cout << auxS;
		return auxS;
	}
};
class session 
	: public std::enable_shared_from_this<session>
{
public:
	session(tcp::socket socket)
		: socket_(std::move(socket))
	{
	}
	char buffsize[128];
	asio::mutable_buffers_1 buff = asio::buffer(buffsize, 128);
	void start()
	{
		do_read();
	}

private:
	void do_read()
	{
		auto self(shared_from_this());
		socket_.async_read_some(asio::buffer(data_, max_length),
			[this, self](std::error_code ec, std::size_t length)
		{
			char ans[20];
			std::string resp = "";
			if (!ec)
			{
				std::cout << data_ << std::endl;
				std::string token;
				std::string httpV, host, function;
				std::pair<std::string, std::string> info;
				if (data_[0] == 'P') {
					token = ExtractInfo::ExtractInfoPost(data_, httpV, host);
					function = host.substr(host.find("/") + 1, host.size());
					info = FM.call_script(function, token);
				}

				if (data_[0] == 'G') {
					token = ExtractInfo::ExtractInfoGet(data_, httpV, host);
					function = host.substr(host.find("/") + 1, host.find("?") - 1);
					function = function.substr(0, function.find("?"));
					info = FM.call_script(function, token);
				}
				resp = resp +
					"HTTP/" + httpV + " " + info.first + "\r\n" +
					"Host: " + host + "\r\n" +
					"Content-Type: text/html\r\n" +
					"Connection: close\r\n" +
					"Accept: */*\r\n" +
					"\r\n" +
					info.second
					+ "\r\n" + "\r\n";
				std::cout << std::endl;
				strcpy(buffsize, resp.c_str());
				std::cout << buffsize << std::endl;


				do_write(resp.length());
			}
		});
	}

	void do_write(int length)
	{
		auto self(shared_from_this());
		asio::async_write(socket_, asio::buffer(buffsize, length),
			[this, self](std::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
			{
				//std::cout <<std::endl<< buffsize;
				//do_read();
			}
		});
	}

	tcp::socket socket_;
	enum { max_length = 1024 };
	char data_[max_length];
};
class server
{
public:
	server(asio::io_service& io_service, short port)
		: acceptor_(io_service, tcp::endpoint(tcp::v4(), port)),
		socket_(io_service)
	{
		do_accept();
	}

private:
	void do_accept()
	{
		acceptor_.async_accept(socket_,
			[this](std::error_code ec)
		{
			if (!ec)
			{
				std::make_shared<session>(std::move(socket_))->start();
			}

			do_accept();
		});
	}

	tcp::acceptor acceptor_;
	tcp::socket socket_;
};

void FunctionsMap::CoutMSize() {
	std::cout << map.size();
}

void StartHTTPServicee(int port) {
	try
	{
		asio::io_service io_service;
		server s(io_service, port);
		io_service.run();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

}