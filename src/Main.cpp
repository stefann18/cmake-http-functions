//#include <iostream>
#include "HTTPEquation.h"
#include <string>
#include <math.h>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <utility>
#include <map>
#include <ctype.h>
#include "asio.hpp"



class Process {
	struct nod {
		char info;
		int prioritate;
		nod *leg;
	};
	struct stiva {
		nod *varf;
	};
	bool Push(stiva *s, char info, int prioritate) {
		if (nod *x = new nod) {
			x->info = info;
			x->prioritate = prioritate;

			x->leg = s->varf;
			s->varf = x;
			return true;
		}
		else return false;
	}
	char Pop(stiva *s) {
		if (s->varf == NULL) {
			return ' ';
		}
		else {
			nod *y;
			y = s->varf;
			s->varf = s->varf->leg;
			return y->info;
		}
	}
	int Pop2(stiva *s) {
		if (s->varf == NULL) {
			return 0;
		}
		else {
			nod *y;
			y = s->varf;
			s->varf = s->varf->leg;
			return y->prioritate;
		}
	}
	int xOpY(int x, int y, char op) {
		if (op == '+')return x + y;
		else if (op == '-')return x - y;
		else if (op == '*')return x*y;
		else if (op == '/') {
			if (y != 0)
				return x / y;
		}
		else if (op == '^')return (int)pow(x, y);
		else return 0;
	}
public:
	std::string Equation(std::string ec) {
		if (ec.length() == 0)return "NO Expression";
		stiva aux;
		char **f;
		aux.varf = NULL;
		int numberOfParanthesis = 0, NumbersMinusSigns = 0;
		f = new char*[ec.length()];
		for (int i = 0; i < ec.length(); i++) {
			f[i] = new char[10];
		}
		int k = 0;
		int j = 0;
		int prioritateSi;
		std::string s;
		for (int i = 0; i < ec.length(); i++) {
			if (ec[i] == ' ')return "WHITE SPACE AAAAAAAAAAAAAAAAA";
			if (isalpha(ec[i]))return "Letters";
			s = "";
			j = 0;
			while (ec[i + j] <= '9'&& ec[i + j] >= '0') {
				if (ec[i + j] == ' ')return "WHITE SPACE";
				if (isalpha(ec[i]))return "Letters";
				s += ec[i + j];
				j++;
			}
			if (strchr("+-*/^", ec[i]) != NULL) {
				NumbersMinusSigns--;
				prioritateSi = 0;
				if (ec[i] == '+' || ec[i] == '-')prioritateSi = 2;
				if (ec[i] == '*' || ec[i] == '/')prioritateSi = 3;
				if (ec[i] == '^')prioritateSi = 4;

				while (aux.varf != NULL && aux.varf->prioritate >= prioritateSi) {
					f[k][0] = (Pop(&aux));
					k++;
				}
				Push(&aux, ec[i], prioritateSi);
			}
			else if (ec[i] == '(') {
				numberOfParanthesis++;
				Push(&aux, ec[i], 1);
			}
			else if (ec[i] == ')') {
				numberOfParanthesis--;
				while (aux.varf != NULL && aux.varf->info != '(') {
					f[k][0] = Pop(&aux);
					k++;
				}
				if (aux.varf != NULL) {
					if (aux.varf->info == '(') {
						Pop(&aux);
					}
				}
				else {
					return "MISING (";
				}
			}
			else {
				NumbersMinusSigns++;
				for (int l = 0; l < s.length(); l++) {
					f[k][l] = s[l];
				}
				k++;
				i += j - 1;

			}
		}
		if (numberOfParanthesis != 0)return "MISSING )";
		if (NumbersMinusSigns != 1)return "OP miss";

		char aux2 = Pop(&aux);
		while (aux2 != ' ') {
			f[k][0] = aux2;
			aux2 = Pop(&aux);
			k++;
		}


		int x, y;
		for (int i = 0; i < k; i++) {
			if (f[i][0] <= '9' && f[i][0] >= '0') {
				Push(&aux, ' ', atoi(f[i]));
			}
			else {
				y = Pop2(&aux);
				x = Pop2(&aux);
				if (f[i][0] == '/' && y == 0)return "Division by 0";
				Push(&aux, ' ', xOpY(x, y, f[i][0]));
			}
		}
		return std::to_string(Pop2(&aux));
	}
};
Process ProcessInstance;

std::pair<std::string, std::string> Equation(std::string token)
{
	std::pair<std::string, std::string> p;
	if (token == "No data sent , or not sent properly") {
		p.first = "404";
		p.second = "No data sent , or not sent properly";
		return p;
	}
	else {
		std::string processResp = ProcessInstance.Equation(token);
		std::string Code;
		if (processResp[0] == 'M' || processResp[0] == 'D' || processResp[0] == 'O' || processResp[0] == 'N'
			|| processResp[0] == 'W' || processResp[0] == 'L') {
			Code = "400";
		}
		else {
			Code = "200 OK";
		}
		p.first = Code; p.second = processResp;
		return p;
	}
}
std::pair<std::string, std::string> Test(std::string token) {
	std::pair<std::string, std::string> pair;
	pair.second = token + " was Tested";
	pair.first = "200 OK";
	return pair;
}

int main()
{
	FM.map.insert(std::make_pair("Equation", &Equation));
	FM.map.insert(std::make_pair("Test", &Test));
	StartHTTPServicee(8080);

	return 0;
}