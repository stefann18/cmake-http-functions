#pragma once
#include <string>
#include <math.h>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <utility>
#include <map>
#include <ctype.h>

typedef std::pair<std::string, std::string>(*ScriptFunction)(std::string); // function pointer type
typedef std::map<std::string, ScriptFunction> script_map;



class FunctionsMap {
public:
	script_map map;
	std::pair<std::string, std::string> call_script(const std::string& pFunction, std::string token);
	void CoutMSize();
};
extern FunctionsMap FM;

void StartHTTPServicee(int port);